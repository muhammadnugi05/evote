@extends('layout.master')
@section('header')
    Tambah Data DPT
@endsection
@section('aset')
<script src="../../../../global_assets/js/plugins/forms/styling/uniform.min.js"></script>

<script src="assets/js/app.js"></script>
<script src="../../../../global_assets/js/demo_pages/form_inputs.js"></script>
@endsection
@section('PageName')
Tambah Data Pemilih Tetap 
@endsection
@section('content')
    <div class="card mt-2 ml-2 mr-2">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">Masukkan Data Pemilih Tetap</h5>
        </div>
        <div class="card-body">
            <form action="#">
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Nama Lengkap</label>
                    <div class="col-lg-10">
                        <input type="text" placeholder="Masukkan Nama" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">NIM</label>
                    <div class="col-lg-10">
                        <input type="text" placeholder="Masukkan Nomor Induk Mahasiswa" class="form-control">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Program Studi</label>
                    <div class="col-lg-10">
                        <select class="form-control">
                            <option value="opt1">Keselamatan dan Kesehatan Kerja</option>
                            <option value="opt1">Manajemen Sumber Daya Manusia</option>
                            <option value="opt2">Relasi Industri</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Tahun Angkatan</label>
                    <div class="col-lg-10">
                        <select class="form-control">
                            <option value="opt1">2017</option>
                            <option value="opt2">2018</option>
                            <option value="opt3">2019</option>
                            <option value="opt4">2020</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label class="col-form-label col-lg-2">Jenis Kelamin</label>
                    <div class="col-lg-10">
                        <select class="form-control">
                            <option value="opt1">Laki-laki</option>
                            <option value="opt2">Perempuan</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-2">Alamat Email</label>
                    <div class="col-md-10">
                        <input class="form-control" placeholder="Masukkan Alamat Email" type="text" name="number">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-2">Nomor Telepon</label>
                    <div class="col-md-10">
                        <input class="form-control" placeholder="Masukkan Nomor Telepon" type="number" name="number">
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
                </div>
            </form>
        </div>
    </div>
@endsection