@extends('layout.master')
@section('header')
Dashboard
@endsection
@section('aset')
<script src="../../../../global_assets/js/plugins/visualization/echarts/echarts.min.js"></script>

<script src="assets/js/app.js"></script>
<script src="../../../../global_assets/js/demo_pages/charts/echarts/pies_donuts.js"></script>
@endsection
@section('content')
    <div class="row mr-2 ml-2 mt-2">
        <div class="col-4">
            <div class="card bg-blue-400">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-around">
                        <div>
                            <i class="icon-user-lock icon-2x"></i>
                        </div>
                    
                        <div>
                            <h1 class="">300</h1>
                            <h5 class="font-weight-bold">Total Pemilih Tetap</h5>
                        </div>
                        <button type="button" class="btn btn-light" ><i class="icon-info22"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card bg-teal-400">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-around">
                        <div>
                            <i class="icon-user icon-2x"></i>
                        </div>
                    
                        <div>
                            <h1 class="">300</h1>
                            <h5 class="font-weight-bold">Sudah Memilih</h5>
                        </div>
                        <button type="button" class="btn btn-light" ><i class="icon-info22"></i></button>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="col-4">
            <div class="card bg-pink-400">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-around">
                        <div>
                            <i class="icon-user-minus icon-2x"></i>
                        </div>
                    
                        <div>
                            <h1 class="">300</h1>
                            <h5 class="font-weight-bold">Belum Memilih</h5>
                        </div>

                        <button type="button" class="btn btn-light" ><i class="icon-info22"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">HASIL PERHITUNGAN SUARA</h5>
                </div>
        
                <div class="card-body">
                    <div class="chart-container">
                        <div class="chart has-fixed-height" id="pie_basic" style="" ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection