@extends('layout.master')

@section("header")
    User Setting
@endsection

@section('aset')
<script src="../../../../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script src="../../../../global_assets/js/plugins/forms/selects/select2.min.js"></script>

<script src="assets/js/app.js"></script>
<script src="../../../../global_assets/js/demo_pages/datatables_basic.js"></script>

@endsection

@section('PageName')
User Setting
@endsection

@section('content')
<div class="card mr-2 ml-2 mt-2">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Admin</h5>
        <div>
            <a href="/pengguna" type="button" class="btn btn-success"><i class="icon-add mr-2"> </i>Tambah Admin</a>
        </div>
    </div>

    <div class="card-body">
    </div>
    
    <table class="table datatable-pagination">
        <thead>
            <tr>
                <th>Nama Lengkap</th>
                <th>Username</th>
                <th>Password</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Marth</td>
                <td>nugieeee</td>
                <td>wkkkkwk</td>
                <td class="text-center">
                    <a href="" type="button" class="btn btn-success btn-icon"><i class="icon-pencil7"></i></a>
                    <a href="" type="button" class="btn btn-danger btn-icon ml-2 mr-2"><i class="icon-trash"></i></a>
                    <a href="" type="button" class="btn btn-primary btn-icon"><i class="icon-info3"></i></a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection