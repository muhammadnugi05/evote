@extends('layout.master')

@section("header")
    DPT Relasi Industri
@endsection

@section('aset')
<script src="../../../../global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
<script src="../../../../global_assets/js/plugins/forms/selects/select2.min.js"></script>

<script src="assets/js/app.js"></script>
<script src="../../../../global_assets/js/demo_pages/datatables_basic.js"></script>

@endsection

@section('PageName')
Data Pemilih Tetap Program Studi Relasi Industri
@endsection

@section('content')
<div class="card mr-2 ml-2 mt-2">
    <div class="card-header header-elements-inline">
        <h5 class="card-title">Daftar Pemilih Tetap Program Studi Relasi Industri</h5>
        <div>
            <a href="/tambahdata" type="button" class="btn btn-success"><i class="icon-add mr-2"> </i>Tambah Data</a>
        </div>
    </div>

    <div class="card-body">
    </div>
    
    <table class="table datatable-pagination">
        <thead>
            <tr>
                <th>Nama Lengkap</th>
                <th>NIM</th>
                <th>Jenis Kelamin</th>
                <th>Tahun Angkatan</th>
                <th>Kode Akses</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Marth</td>
                <td>022020023</td>
                <td>Laki-laki</td>
                <td>2017</td>
                <td><div class="btn btn-success">Lihat Kode</div></td>
                <td class="d-flex justify-content-around">
                    <a href="" type="button" class="btn btn-success btn-icon"><i class="icon-pencil7"></i></a>
                    <a href="" type="button" class="btn btn-danger btn-icon ml-2 mr-2"><i class="icon-trash"></i></a>
                    <a href="" type="button" class="btn btn-primary btn-icon"><i class="icon-info3"></i></a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection