<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/tambahdata', function () {
    return view('create');
});
Route::get('/pengguna', function () {
    return view('pengguna.main');
});
Route::get('/', function () {
    return view('dashboard');
});
Route::get('/dptri', function () {
    return view('dptRI.main');
});
Route::get('/dptk3', function () {
    return view('dptK3.main');
});
Route::get('/dptmsdm', function () {
    return view('dptMSDM.main');
});
